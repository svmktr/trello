const apiKey = '###################################'
const token = '#############################################'

url = `https://api.trello.com/1/boards/5e0b0a1f83f23a2fedcf209c/lists?cards=all&key=${apiKey}&token=${token}`
// const link = 'https://trello.com/b/B6Vp82qf/shivam-katiyar'

async function get() {
    await fetch(url)
        .then(response => {
            if (response.ok) {
                return response.json()
            } else {
                console.log('Error')
            }
        }).then(jsonResponse => {
            // console.log(jsonResponse)
            createLists(jsonResponse)
        })
}
get();
// const mainDiv = document.getElementById('main-div');
function createLists(listsAndCards) {
    // console.log( listsAndCards);  
    let mainDiv = document.getElementById('main-div')
    while (mainDiv.hasChildNodes()) {
        mainDiv.removeChild(mainDiv.firstChild);
    }
    for (listName of listsAndCards) {
        let listDiv = document.createElement('div');
        listDiv.className = 'list-div';
        mainDiv.appendChild(listDiv);
        // console.log(listName.name);
        const text = document.createTextNode(listName.name);
        // const mainDiv = document.getElementById('items');
        let ul = document.createElement('ul');
        ul.className = 'ul';
        let textArea = document.createElement('textarea')
        textArea.className = 'text-area'
        textArea.appendChild(text)
        ul.appendChild(textArea);
        // console.log(listName.id)
        ul.setAttribute('listId', listName.id);
        // console.log(ul.getAttribute('listId'));
        for (card of listName.cards) {
            // console.log(card.id)
            let li = document.createElement('li');
            li.className = 'cards';
            li.append(card.name)
            li.setAttribute('cardId', card.id)
            let delBtn = document.createElement('i')
            delBtn.className = "fa fa-trash del-btn";
            // delBtn.appendChild(doc
            li.appendChild(delBtn)
            delBtn.addEventListener('click', delCard)
            let editBtn = document.createElement('i')
            editBtn.className = 'edit-btn material-icons';
            editBtn.appendChild(document.createTextNode('border_color'))
            li.appendChild(editBtn)
            editBtn.addEventListener('click', editCard)
            li.addEventListener('click', openPopUp)
            ul.appendChild(li);
        }

        // Adding Add-button:
        addCardBtn = document.createElement('button')
        addCardBtn.className = 'addCardBtn'
        addCardBtn.appendChild(document.createTextNode('AddItem'))
        // let addBtn = document.getElementsByClassName('addCardBtn')
        addCardBtn.addEventListener('click', inputCard)
        // Adding card to ul:
        ul.appendChild(addCardBtn)
        // Adding ul to main div:
        listDiv.appendChild(ul);
    }

    // let addlist = document.createElement('div')
    // addlist.className = "add-list"
    let listbtn = document.createElement('button')
    listbtn.className = 'list-btn'
    listbtn.appendChild(document.createTextNode('Add New List'))
    mainDiv.append(listbtn)
    listbtn.addEventListener('click', addlist)

}



function inputCard(e) {
    // console.log(e);
    const form = document.createElement('form');
    newCard = document.createElement('input')
    submitBtn = document.createElement('button')
    newCard.className = 'card-input'
    // ul.appendChild(newCard)
    // console.log(e.srcElement.parentElement);
    e.srcElement.style = 'display:none'
    form.append(newCard)
    // submitBtn = document.createElement('button')
    submitBtn.className = 'submit-card'
    submitBtn.appendChild(document.createTextNode('submit'))
    form.append(submitBtn)

    e.srcElement.parentElement.append(form)
    form.addEventListener('submit', addcard);
    newCard.focus();
    // e.srcElement.parentElement.append(submit)
}



async function addcard(e) {
    // console.log('hi')
    e.preventDefault();
    e.stopPropagation();
    // console.log(e.srcElement.parentElement);
    list = e.srcElement.parentElement;
    listId = list.getAttribute('listId');
    // console.log(e.srcElement.firstChild.value);
    // console.log(listId)
    input = e.srcElement.firstChild.value;
    // console.log(input)
    if (input) {
        const Url = `https://api.trello.com/1/cards?idList=${listId}&name=${input}&key=${apiKey}&token=${token}`;
        const resp = await fetch(Url, {
            method: 'POST'
        });
        if (resp.ok) {
            get();
        }
    }
}


async function delCard(e) {
    e.preventDefault();
    e.stopPropagation();
    // console.log(e.srcElement.parentElement)
    const cardId = e.srcElement.parentElement.getAttribute('cardId');
    const deleteCardUrl = `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`;
    const resp = await fetch(deleteCardUrl, {
        method: 'DELETE'
    });
    if (resp.ok) {
        get();
    }
}


async function editCard(e) {
    submitBtn = document.getElementsByClassName('addCardBtn');
    // console.log(submitBtn)
    // console.log(e.srcElement.parentElement)
    let card = e.srcElement.parentElement;
    // card.style = 'display:block';
    e.preventDefault();
    e.stopPropagation();
    const cardName = e.srcElement.parentElement;
    const form = document.createElement('form');
    const input = document.createElement('input');
    form.append(input);
    cardName.append(form);
    input.focus();
    form.addEventListener('submit', async function (e) {
        e.preventDefault();
        e.stopPropagation();
        const input = e.srcElement.firstChild.value;
        cardId = e.srcElement.parentElement.getAttribute('cardId');
        if (input) {
            const Url = `https://api.trello.com/1/cards/${cardId}?name=${input}&key=${apiKey}&token=${token}`;
            const resp = await fetch(Url, {
                method: 'PUT'
            });
            if (resp.ok) {
                get();
            }
        }
    })
}

async function addlist(e) {
    // console.log('hihihihi')
    // console.log(e.srcElement)
    e.srcElement.style = 'display:none';
    const form = document.createElement('form')
    const input = document.createElement('input');
    form.append(input)
    // console.log(e.srcElement.parentElement)
    // e.srcElement.parentElement.append(form);
    e.srcElement.parentElement.append(form);
    input.focus();
    form.addEventListener('submit', async function (e) {
        e.preventDefault();
        e.stopPropagation();
        // console.log(e.srcElement.parentElement);
        // console.log(e.srcElement.firstChild.vlaue);
        const input = e.srcElement.firstChild.value;
        // input.focus();
        if (input) {
            const Url = `https://api.trello.com/1/lists?name=${input}&idBoard=5e0b0a1f83f23a2fedcf209c&pos=bottom&key=${apiKey}&token=${token}`;
            const resp = await fetch(Url, {
                method: 'POST'
            });
            if (resp.ok) {
                get();
            }
        }
    })

}

function openPopUp(e) {
    e.stopPropagation();
    let card = e.srcElement;
    let cardId = card.getAttribute('cardId')
    popupbox(cardId)
    // console.log(cardId)
}

async function popupbox(cardId) {
    let mainDiv = document.getElementById('main-div')
    while (mainDiv.lastChild.getAttribute('class') === 'popup-Box') {
        mainDiv.lastChild.remove();
    }
    // console.log()

    let listDiv = document.getElementsByClassName('list-div')
    for (let i of listDiv) {
        i.style.opacity = "0.5";
    }
    // console.log(listDiv[0])

    popupDiv = document.createElement('div')
    popupDiv.className = 'popup-Box';
    popupDiv.setAttribute('card_id', cardId);
    popupDiv.style.display = 'block';
    // popupDiv.style = 'position:fixed';
    let delBtn = document.createElement('button')
    delBtn.className = 'close-btn'
    delBtn.appendChild(document.createTextNode('close'))
    delBtn.addEventListener('click', closePopup)
    popupDiv.append(delBtn)

    let url = `https://api.trello.com/1/cards/${cardId}/checklists?checkItems=all&checkItem_fields=name%2CnameData%2Cpos%2Cstate&filter=all&fields=all&key=${apiKey}&token=${token}`
    const res = await fetch(url, {
        method: 'GET'
    });
    // console.log('after reqss')
    // console.log(res.ok)
    if (res.ok) {
        const data_json = await res.json();
        checklist(data_json)
    }


    function checklist(data_json) {
        // console.log(data_json)
        for (let listName of data_json) {
            // // let taskName = data_json
            // console.log(listName.id)

            let checkBox = document.createElement('div')
            let delBtn = document.createElement('button')
            delBtn.className = 'del-checklist'
            delBtn.appendChild(document.createTextNode('X'))
            delBtn.addEventListener('click', delcheckList)
            checkBox.className = 'Checklist-name'
            checkBox.append(listName.name)
            checkBox.append(delBtn)
            checkBox.setAttribute('checkListId', listName.id)
            // console.log(checkBox);

            for (task of listName.checkItems) {
                // console.log(task.id)

                let newTask = document.createElement('div');
                newTask.className = 'task';
                let check = document.createElement('input')
                check.type = 'checkbox'
                if (task.state === 'complete') {
                    check.checked = task.state;
                    newTask.style.setProperty('text-decoration', 'line-through');
                }
                check.addEventListener('click', strikeThrough)
                // let addItem = document.createElement('button');
                // addItem.className = 'add-item'
                // addItem.appendChild(document.createTextNode('X'))

                // // check.checked
                newTask.setAttribute('taskId', task.id)
                newTask.append(check)
                newTask.append(task.name)
                // newTask.append()
                checkBox.appendChild(newTask)
                popupDiv.appendChild(checkBox)
            }
            // console.log(e.srcElement.attributes[1].value)
            let addItemBtn = document.createElement('button')
            addItemBtn.className = 'add-item'
            addItemBtn.append(document.createTextNode('+Add Item'))
            checkBox.appendChild(addItemBtn)
            popupDiv.appendChild(checkBox);
            addItemBtn.addEventListener('click', addItem)
        }
    }
    let addListBtn = document.createElement('button')
    addListBtn.className = 'add-checklist'
    addListBtn.append(document.createTextNode('+Add CheckList'))
    addListBtn.addEventListener('click', addCheckList)
    popupDiv.appendChild(addListBtn)
    mainDiv.appendChild(popupDiv);
    // console.log(card)
}

async function addCheckList(e) {
    let cardId = e.srcElement.parentElement.getAttribute('card_id')
    // console.log(cardId)
    e.srcElement.style = 'display:none';
    const form = document.createElement('form');
    const input = document.createElement('input');
    form.append(input)
    e.srcElement.parentElement.append(form);
    input.focus();
    form.addEventListener('submit', async function (e) {
        // card1 = e.srcElement.parentElement.parentElement;
        // console.log(card1)
        e.preventDefault();
        e.stopPropagation();
        const text = input.value
        // console.log(text)
        if (text) {
            const url = `https://api.trello.com/1/checklists?idCard=${cardId}&name=${text}&key=${apiKey}&token=${token}`;
            // console.log(cardId)
            await fetch(url, {
                method: 'POST'
            }).then(() => {
                // card = e.srcElement.parentElement.parentElement;
                // console.log(cardId)
                popupbox(cardId);
            }).catch(err => {
                console.log(err)
            });
        }
    })
}


function addItem(e) {
    e.preventDefault();
    // console.log(e.srcElement.parentElement);
    // console.log(e.srcElement.parentElement.getAttribute('checklistid'));
    const checkList = e.srcElement.parentElement
    const checkListId = e.srcElement.parentElement.getAttribute('checklistid');
    // console.log(checkListId);
    const cardId = e.srcElement.parentElement.parentElement.getAttribute('card_Id');
    // console.log(cardId)
    e.srcElement.style = 'display:none';
    const form = document.createElement('form');
    const input = document.createElement('input');
    form.append(input)
    checkList.append(form);
    input.focus();
    form.addEventListener('submit', async function (e) {
        e.preventDefault();
        const text = input.value;
        // console.log(text)
        if (text) {
            const url = `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${text}&pos=bottom&checked=false&key=${apiKey}&token=${token}`;
            const resp = await fetch(url, {
                method: 'POST'
            });
            if (resp.ok) {
                popupbox(cardId);
            }
        }
    })
}

async function delcheckList(e) {
    // console.log('hi')
    // console.log(e.srcElement.parentElement.getAttribute('checklistid'));
    const checkListId = e.srcElement.parentElement.getAttribute('checklistid');
    const cardId = e.srcElement.parentElement.parentElement.getAttribute('card_Id');
    // console.log(cardId)
    const url = `https://api.trello.com/1/checklists/${checkListId}?key=${apiKey}&token=${token}`;
    const resp = await fetch(url, {
        method: 'DELETE'
    });
    if (resp.ok) {
        popupbox(cardId);
    }
}

async function strikeThrough(e) {
    // console.log('hi')
    e.preventDefault();
    let taskId = e.srcElement.parentElement.getAttribute('taskId');
    const cardId = e.srcElement.parentElement.parentElement.parentElement.getAttribute('card_id');
    // console.log(cardId)
    // const idCheckItem = e.srcElement.parentElement.getAttribute('idCheckitem');
    let state;
    if (event.target.checked === false) {
        state = 'incomplete';
    } else if (event.target.checked === true) {
        state = 'complete';
    }
    const UpdateCheckItemUrl = `https://api.trello.com/1/cards/${cardId}/checkItem/${taskId}?state=${state}&key=${apiKey}&token=${token}`;

    const resp = await fetch(UpdateCheckItemUrl, {
        method: 'PUT'
    });
    if (resp.ok) {
        popupbox(cardId);
    }
}



// window.onclick = e => {
//     // console.log('onclick')
//     const checklist = document.getElementsByClassName("popup-Box")[0];
//     // const checklist = document.getElementsByClassName("Checklist-name")[0];
//     // console.log(checklist)
//     // console.log( e.target.parentElement.parentElement.parentElement)
//     if (!(e.target === checklist || e.target.parentElement === checklist || e.target.parentElement.parentElement === checklist || e.target.parentElement.parentElement.parentElement === checklist)) {
//         // checklist.style.display = 'none';
//     // //     // remove the card div
//         // console.log('out onclick')
//         const parent = checklist.parentElement;
//     //     // console.log(parent);
//         parent.removeChild(checklist);
//         // for(i in document.getElementsByClassName('list')){
//             let listDiv = document.getElementsByClassName('list-div')
//             for (let i of listDiv){
//                 i.style.opacity = "1";
//             }
//             // document.getElementsByClassName('list-div')[0].style.opacity = "1"
//             // console.log(document.getElementsByClassName('list')[i]);
//         // }
//     }
// }


function closePopup(e) {
    // console.log(e.srcElement.parentElement.parentElement)
    let child = document.getElementsByClassName('popup-Box')[0];
    // console.log(child)
    let parent = document.getElementById('main-div');
    parent.removeChild(child)
    let listDiv = document.getElementsByClassName('list-div')
    for (let i of listDiv) {
        i.style.opacity = "1";
    }
}